﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> where T : class, new()
{
    protected Singleton()
    {
        Debug.Assert(instance == null);
    }
    protected static T instance;
    static Singleton() {
        instance = new T();
    }

    public static T Instance
    {
        get
        {
            return instance;
        }
    }
}

public class GameState : Singleton<GameState>
{
    int[] _ModelNum = new int[4];
    public int PlayerNum { get; set; }
    public int[] ModelNum
    {
        get { return _ModelNum; }
        set { _ModelNum = value; }
    }
//<<<<<<< HEAD

    public int[] ControllerNum = { 1, 2, 3, 4 };

//=======
    public int[] Score { get; set; }
//>>>>>>> a143e5aa2b6b55f95a83e284c4fb2e4e2f290211
}