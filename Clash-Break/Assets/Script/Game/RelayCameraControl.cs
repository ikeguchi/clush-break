﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelayCameraControl : MonoBehaviour
{
    private Camera _camera;
    private int _targetnum;
	// Use this for initialization
	void Awake ()
	{
	    _camera = GetComponent<Camera>();
	    _camera.enabled = false;
	    enabled = false;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		SetRotate();
	}

    public void SetCamera(int playernum, RenderTexture texture)
    {
        _camera.enabled = true;
        _camera.targetTexture = texture;
        _targetnum = playernum;
    }

    public void SetCamera(int playernum)
    {
        _targetnum = playernum;
    }

    public void ReleaseCamera()
    {
        _camera.targetTexture = null;
        _camera.enabled = false;
        enabled = false;
    }

    void SetRotate()
    {
        Transform player = GameInfo.Instance.Player[_targetnum].transform;
        if (player != null)
            transform.LookAt(player);
    }
}
