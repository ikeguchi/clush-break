﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperControl : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        AudioControlinGame.Instance.PlaySE(SEList.Bumper);
        PlayerControl player = other.gameObject.GetComponent<PlayerControl>();
        if (!player.Invulnerable)
            player.Clashed(0.5f);
    }
}