﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateObject : MonoBehaviour {

    public GameObject[] Models;
    public GameObject Camera, Bar;
    public GameObject[] Effects;
    public static InstantiateObject Instance;
    public GameObject[] Cameras { get; set; }
    [SerializeField] private GameObject GameEndPanel;
    Vector3[] pos = { new Vector3(10.0f, 1.5f, 0.0f), new Vector3(-10.0f, 1.5f, 0.0f), new Vector3(0.0f, 1.5f, 10.0f), new Vector3(10.0f, 1.5f, -10.0f) };
    Quaternion[] quat = { Quaternion.Euler(0.0f, -90.0f, 0.0f), Quaternion.Euler(0.0f, 90.0f, 0.0f), Quaternion.Euler(0.0f, 180.0f, 0.0f), Quaternion.Euler(0.0f, 0.0f, 0.0f) };
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    private void OnDestroy()
    {
        Instance = null;
    }
    // Use this for initialization
    void Start () {
        if (GameState.Instance.PlayerNum == 2)
            Bar.SetActive(false);
        Cameras = new GameObject[GameState.Instance.PlayerNum];
        GameInfo.Instance.init();
        GameState.Instance.Score = new int[GameState.Instance.PlayerNum];
        for (int i = 0; i < GameState.Instance.PlayerNum; i++)
        {
            GameState.Instance.Score[i] = 0;
            InitPlayer(i);
        }
        GameObject.FindGameObjectWithTag("Timer").GetComponent<TimerControl>().StartCount(60.0f);
    }

    void InitPlayer(int playernum)
    {
        GameObject obj = Instantiate(Models[GameState.Instance.ModelNum[playernum]], pos[playernum], quat[playernum], GameInfo.Instance.PausableObject);
        obj.GetComponent<PlayerControl>().SetPlayerNum(playernum);
        Cameras[playernum] = Instantiate(Camera, GameInfo.Instance.PausableObject);
        Cameras[playernum].GetComponent<CameraControl>().SetCam(playernum, obj);
    }

    /*void ResetPlayer(int playernum)
    {
        GameObject obj = Instantiate(Models[GameState.Instance.ModelNum[playernum]], pos[playernum], quat[playernum], GameInfo.Instance.PausableObject);
        obj.GetComponent<PlayerControl>().SetPlayerNum(playernum);
        Cameras[playernum].GetComponent<CameraControl>().ReSetCam(obj);
    }*/

    public void Reset(int playernum,out Vector3 _pos, out Quaternion _quat)
    {
        _pos = pos[playernum];
        _quat = quat[playernum];
    }

    /*IEnumerator DelayMethod(float waitTime, System.Action action)
    {
        yield return new WaitForSeconds(waitTime);
        action();
    }*/

    public void CreateEffect(EffectType type, Vector3 pos, Quaternion quat)
    {
        Instantiate(Effects[(int)type], pos, quat, transform);
    }

    public void GameStart()
    {
        for (int i = 0; i < GameState.Instance.PlayerNum; i++)
        {
            GameInfo.Instance.Player[i].enabled = true;
        }
    }

    public void GameEnd()
    {
        for (int i = 0; i < GameState.Instance.PlayerNum; i++)
        {
            GameInfo.Instance.Player[i].Stop();
        }
        int[] rank = new int[GameState.Instance.PlayerNum];
        for (int i = 0; i < GameState.Instance.PlayerNum; i++)
        {
            rank[i] = 1;
        }
        for (int i = 0; i < GameState.Instance.PlayerNum; i++)
        {
            for (int j = 0; j < GameState.Instance.PlayerNum; j++)
            {
                if (i != j && GameState.Instance.Score[i] < GameState.Instance.Score[j])
                    rank[i]++;
            }
        }
        for (int i = 0; i < GameState.Instance.PlayerNum; i++)
        {
            Cameras[i].GetComponent<CameraControl>().Rank(rank[i]);
        }
        ChangeBGM.Instance.Change();
        GameEndPanel.SetActive(true);
    }
}

public enum EffectType
{
    ClashEffect,
    StrongClashEffect
}
