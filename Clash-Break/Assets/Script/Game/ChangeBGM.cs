﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBGM : MonoBehaviour
{
    public static ChangeBGM Instance;
    private Animation _changeAnimation;
	// Use this for initialization
	void Start ()
	{
	    _changeAnimation = GetComponent<Animation>();
	    if (Instance==null)
	    {
	        Instance = this;
	    }
	}

    public void Change()
    {
        _changeAnimation.Play();
    }
}
