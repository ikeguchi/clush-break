﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectControl : MonoBehaviour
{
    private float time = 0.0f;
	// Use this for initialization
    private void Update()
    {
        if(time>1.0f)
            Destroy(gameObject);
        time += Time.deltaTime;
    }
}
