﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashPanelControl : MonoBehaviour {
    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerControl>();
        if (!player.IsClashMaterial)
        {
            player.Rg.velocity += transform.right * 100.0f;
            player.IsClashMaterial = true;
            AudioControlinGame.Instance.PlaySE(SEList.DashPanel);
        }
    }
}
