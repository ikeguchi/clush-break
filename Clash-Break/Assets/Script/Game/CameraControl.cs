﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour
{
    string KeyName;
    Camera cam;
    [SerializeField] private Camera UIcam;
    private float width, radius, nowangle, height;
    int playernum;
    Transform parent;
    //public GameObject kara;
    [SerializeField] private Sprite[] _itemImages;
    Image ItemFrame;
    [SerializeField] private Image HavingItemImage;
    [SerializeField] private Text score;
    [SerializeField] private EffectUI EffectImage;
    [SerializeField] private Image RankImage;
    [SerializeField] private SSControl ssimage;
    Vector3 velocity = Vector3.zero;

    public Transform Player { get; set; }

    // Use this for initialization
    void Awake()
    {
        cam = GetComponent<Camera>();
        width = GameState.Instance.PlayerNum == 2 ? 1.000f : 0.500f;
        //parent = transform.parent;
        radius = -15.0f;
        nowangle = 0.0f;
        height = 4.0f;
        ssimage.enabled = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Move();
    }

    void Update()
    {
        WriteUI();

    }

    public void SetCam(int num, GameObject player)
    {
        float x = 0.000f;
        float y = 0.000f;
        if ((num <= 1 && GameState.Instance.PlayerNum > 2) || (num == 0 && GameState.Instance.PlayerNum == 2))
            y = 0.509f;
        if ((num == 1 || num == 3) && GameState.Instance.PlayerNum != 2)
            x = 0.500f;
        Rect rec = new Rect(x, y, width, 0.491f);
        cam.rect = rec;
        UIcam.rect = rec;
        KeyName = "CHorizontal" + GameState.Instance.ControllerNum[num];
        Player = player.transform;
        Rotate(0.0f);
        playernum = num;
        ItemFrame = HavingItemImage.transform.parent.GetComponent<Image>();
        ItemFrame.sprite = Resources.Load<Sprite>("Images/" + (num + 1) + "PFrame");
    }
    public void ReSetCam(GameObject player)
    {
        Player = player.transform;
        Rotate(0.0f);
    }

    void Move()
    {
        if (Player == null)
            return;
        //parent.position = Vector3.MoveTowards(parent.position, Player.position, 100.0f * Time.deltaTime);
        //parent.position = Player.position;
        //transform.position = Player.position;
        float horizontal = Input.GetAxis(KeyName);
        if (Mathf.Abs(horizontal) > 0.0f)
            Rotate(-horizontal * 5.0f);
        else
            Rotate(Mathf.Lerp(0.0f, nowangle, 5.0f * Time.deltaTime));

    }

    void Rotate(float angle)
    {
        float playerangle = Player.localEulerAngles.y;
        nowangle = Mathf.Clamp(nowangle - angle, -180.0f, 180.0f);
        float x = Mathf.Sin((nowangle + playerangle) * Mathf.Deg2Rad) * radius;
        float z = Mathf.Cos((nowangle + playerangle) * Mathf.Deg2Rad) * radius;
        Vector3 pos = new Vector3(x, height, z) + Player.position;
        transform.position = Vector3.MoveTowards(transform.position, pos, 10.0f);
        transform.LookAt(Player);
    }

    void WriteUI()
    {
        ItemType havinItemType;
        List<ItemEffect> effects;
        bool IsDash, IsSSDash;
        int DashCount;
        GameInfo.Instance.Player[playernum].GetStatus().GetUIInfo(out havinItemType, out effects, out IsDash, out IsSSDash, out DashCount);
        HavingItemImage.sprite = _itemImages[(int)havinItemType];
        EffectImage.DrawUI(effects, _itemImages, IsDash, IsSSDash, DashCount);
        score.text = GameState.Instance.Score[playernum].ToString();
    }

    public void Cart3SpecialSkill()
    {
        cam.cullingMask &= ~(1 << LayerMask.NameToLayer("cart3"));
        cam.cullingMask |= (1 << LayerMask.NameToLayer("cart3_" + (playernum + 1)));
    }
    public void Cart3StopSpecialSkill()
    {
        cam.cullingMask |= (1 << LayerMask.NameToLayer("cart3"));
        cam.cullingMask &= ~(1 << LayerMask.NameToLayer("cart3_" + (playernum + 1)));
    }

    public void Rank(int rank)
    {
        RankImage.gameObject.SetActive(true);
        RankImage.sprite = Resources.Load<Sprite>("Images/UI/rank" + rank);
    }

    public void UseSS()
    {
        ssimage.enabled = true;
    }

    public void EndSS()
    {
        ssimage.gameObject.SetActive(false);
    }

}