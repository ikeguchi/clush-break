﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResurrectionLightControl : MonoBehaviour
{
    private ParticleSystem pt;
	// Use this for initialization
	void Awake ()
	{
	    pt = GetComponent<ParticleSystem>();
	    ParticleSystem.MainModule _main = pt.main;
	    _main.startColor = GameInfo.Instance.PlayerColors[GetComponentInParent<PlayerStatus>().PlayerNum];
	}
}
