﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : Singleton<GameInfo>
{
    Transform _PausableObject;
    public int PausingPlayer { get; set; }
    public Transform PausableObject
    {
        get
        {
            if (_PausableObject == null)
            {
                _PausableObject = GameObject.FindGameObjectWithTag("PausableObject").transform;
                return _PausableObject;
            }
            return _PausableObject;
        }
    }
    public PlayerControl[] Player { get; set; }
    public bool[] isSSuse { get; set; }
    public bool isGamePlaying { get; set; }
    public int[] attackplayernum { get; set; }
    public float RemainingTime { get; set; }

    public Color[] PlayerColors =
    {
        new Color(1, 0.3f, 0.3f),
        new Color(0.3f, 0.3f, 1),
        new Color(1, 1, 0.3f),
        new Color(0.3f, 1, 0.3f)
    };
    public void init()
    {
        Player = new PlayerControl[GameState.Instance.PlayerNum];
        isSSuse = new bool[GameState.Instance.PlayerNum];
        attackplayernum = new int[GameState.Instance.PlayerNum];
    }
}
