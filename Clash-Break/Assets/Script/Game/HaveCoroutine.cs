﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HaveCoroutine : MonoBehaviour
{
    private readonly List<Coroutine> _playingCoroutine = new List<Coroutine>();
    class Coroutine
    {
        public Coroutine(IEnumerator routine, int id)
        {
            this.Routine = routine;
            this.Id = id;
        }
        public readonly IEnumerator Routine;
        public readonly int Id;
    }

    public void StartCoroutineAndSave(IEnumerator routine, int id)
    {
        if (_playingCoroutine.Find(r => r.Id == id) != null)
        {
            foreach (var r in _playingCoroutine.FindAll(r => r.Id == id))
            {
                StopCoroutine(r.Routine);
            }
            _playingCoroutine.RemoveAll(r => r.Id == id);
        }
        Coroutine _routine = new Coroutine(routine, id);
        _playingCoroutine.Add(_routine);
        StartCoroutine(_routine.Routine);
    }

    public void StopCoroutineAndRemove(int id)
    {
        Coroutine co = _playingCoroutine.Find(routine => routine.Id == id);
        if (co != null)
        {
            StopCoroutine(co.Routine);
            _playingCoroutine.Remove(co);
        }

    }

    public bool FindCoroutine(int id)
    {
        if (_playingCoroutine.Find(r => r.Id == id) != null)
            return true;
        return false;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnEnable()
    {
        foreach (var coroutine in _playingCoroutine)
        {
            StartCoroutine(coroutine.Routine);
        }
    }
}
