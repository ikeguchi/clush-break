﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AudioControlinGame : MonoBehaviour
{
    public static AudioControlinGame Instance;
    AudioSource _audio;
    private readonly AudioClip[] _se = new AudioClip[11];
    // Use this for initialization
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start ()
    {
        _audio = GetComponent<AudioSource>();
        LoadSE();
    }

    void LoadSE()
    {
        for (int i = 0; i <(int)SEList.Length ; i++)
        {
            _se[i] = Resources.Load<AudioClip>("Audio/SE/"+((SEList)i).ToString());
        }
    }

    public void PlaySE(SEList seList)
    {
        if ((int)seList>_se.Length-1)
        {
            Debug.Log(seList+"が存在しない");
            return;
        }
        _audio.PlayOneShot(_se[(int) seList]);
    }
}

public enum SEList
{
    Itemget,
    Resurrection,
    Shield,
    PowUp,
    PowDown,
    Bumper,
    Bust,
    ShockWave,
    StrongClash,
    WeakClash,
    DashPanel,
    Length
}
