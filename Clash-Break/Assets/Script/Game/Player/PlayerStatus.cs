﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour {
    public float default_rotationality, default_force, default_mass;
    /// <summary>
    /// 0～3
    /// </summary>
    public int PlayerNum { get; set; }
    public int DashCount { get; set; }
    public bool IsDeath { get; set; }
    public bool IsClashed { get; set; }
    public bool IsDash { get; set; }
    public bool IsSSDash { get; set; }
    public bool IsTransparent { get; set; }
    public float mag_rotationality { get; set; }
    public float mag_force { get; set; }
    public float mag_mass { get; set; }
    public float mag_power { get; set; }
    public float mag_defence { get; set; }
    public float rotationality { get { return default_rotationality * mag_rotationality; } }
    public float force { get { return default_force * mag_force; } }
    public float mass { get { return default_mass * mag_mass; } }
    public float power { get { return 1.0f * mag_power * force; } }
    public float defence { get { return 1.0f * mag_defence; } }
    public ItemType HavingItem { get; set; }
    private readonly List<ItemEffect> itemEffects = new List<ItemEffect>();

    public void ParameterReset()
    {
        mag_rotationality = 1.0f;
        mag_force = 1.0f;
        mag_mass = 1.0f;
        mag_power = 1.0f;
        mag_defence = 1.0f;
        DashCount = 5;
        HavingItem = ItemType.None;
        IsDash = false;
        IsSSDash = false;
        IsDeath = false;
        IsClashed = false;
        ClearEffect();
    }

    void Update()
    {
        int i = 0;
        itemEffects.Sort((itemA, itemB) => itemA.RemainingTime.CompareTo(itemB.RemainingTime));
        foreach (var item in itemEffects)
        {
            if (item.ItemUpdate())
                i++;
        }
        if (i > 0)
            itemEffects.RemoveRange(0, i);
    }

    void ClearEffect()
    {
        foreach (var item in itemEffects)
        {
            item.DestroyEffect();
        }
        itemEffects.Clear();
    }

    public void GetEffect(ItemType type)
    {
        switch (type)
        {
            case ItemType.AttackUp:
                AudioControlinGame.Instance.PlaySE(SEList.PowUp);
                itemEffects.Add(new ChangeAttack(1,5.0f,PlayerNum));
                break;
            case ItemType.DefenceUp:
                AudioControlinGame.Instance.PlaySE(SEList.PowUp);
                itemEffects.Add(new ChangeDefence(1,5.0f,PlayerNum));
                break;
            case ItemType.AttackDown:
                AudioControlinGame.Instance.PlaySE(SEList.PowDown);
                foreach (var player in GameInfo.Instance.Player)
                {
                    player.GetStatus().Debuff(ItemType.AttackDown, PlayerNum);
                }
                break;
            case ItemType.DefenceDown:
                AudioControlinGame.Instance.PlaySE(SEList.PowDown);
                foreach (var player in GameInfo.Instance.Player)
                {
                    player.GetStatus().Debuff(ItemType.DefenceDown, PlayerNum);
                }
                break;
            case ItemType.Dash:
                DashCount += 3;
                break;
            case ItemType.Transparent:
                itemEffects.RemoveAll(a => a.Type == ItemType.Transparent);
                itemEffects.Add(new TransparentItem(5.0f,PlayerNum));
                break;
            case ItemType.InkScreen:
                foreach (var player in GameInfo.Instance.Player)
                {
                    player.GetStatus().Debuff(ItemType.InkScreen, PlayerNum);
                }
                break;
            case ItemType.None:
                Debug.LogError("無が使用された");
                break;
        }
    }

    public void Debuff(ItemType type, int playernum)
    {
        if (PlayerNum == playernum || IsDeath)
            return;
        switch (type)
        {
            case ItemType.AttackDown:
                itemEffects.Add(new ChangeAttack(-0.1f, 5.0f, PlayerNum));
                break;
            case ItemType.DefenceDown:
                itemEffects.Add(new ChangeDefence(-0.1f, 5.0f, PlayerNum));
                break;
            case ItemType.InkScreen:
                itemEffects.Add(new InkScreen(8.0f, PlayerNum));
                break;
        }
    }

    public void GetUIInfo(out ItemType havingItemType,out List<ItemEffect> effects,out bool isdash,out bool SSdash,out int dashcount)
    {
        havingItemType = HavingItem;
        effects = itemEffects;
        isdash = IsDash;
        SSdash = IsSSDash;
        dashcount = DashCount;
    }
}

public enum ItemType
{
    AttackUp,
    DefenceUp,
    AttackDown,
    DefenceDown,
    Transparent,
    Dash,
    InkScreen,
    None
}
