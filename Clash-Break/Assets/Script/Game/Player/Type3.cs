﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Type3 : PlayerControl {
    //巨大化の倍率
    float _magnification;

    [SerializeField] private GameObject _ssObject;
    private Vector3 _scale;
    private float _mass;
    private float _force;
    private float _height;
	// Use this for initialization
	void Start () {
        _magnification = 3.0f;
	}

    public override void SetPlayerNum(int num)
    {
        base.SetPlayerNum(num);
        _ssObject.layer = LayerMask.NameToLayer("cart3_" + (num + 1));
    }

    // Update is called once per frame
	void Update () {
        DefaultLoop();
	}

    protected override IEnumerator SpecialSkill()
    {
        return Megamorph();
    }

    void SaveDefaultParam()
    {
        _scale = transform.localScale;
        _mass = status.default_mass;
        _force = status.default_force;
        _height = Height;
    }
    IEnumerator Megamorph()
    {
        float time = 0.0f;
        float magnification;
        float cubedMagni;
        SaveDefaultParam();
        InstantiateObject.Instance.Cameras[status.PlayerNum].GetComponent<CameraControl>().Cart3SpecialSkill();
        while (time < 1.0f)
        {
            magnification = Mathf.Lerp(1.0f, _magnification, time);
            cubedMagni = Mathf.Pow(magnification, 3.0f);
            transform.localScale = magnification * _scale;
            status.default_mass = _mass * cubedMagni;
            rg.mass = status.mass;
            status.default_force = cubedMagni * _force;
            Height = magnification * _height - 0.5f * (magnification - 1.0f);
            //status.mag_rotationality = Mathf.Lerp(1.0f, 100.0f, time);
            time += Time.deltaTime;
            yield return null;
        }
        transform.localScale = _magnification  * _scale;
        cubedMagni=Mathf.Pow(_magnification, 3.0f);
        status.default_mass = _mass * cubedMagni;
        rg.mass = status.mass;
        status.default_force = _force * cubedMagni;
        Height = _height * _magnification - 0.5f * (_magnification - 1.0f);
        status.mag_rotationality = 200.0f;
        yield return new WaitForSeconds(5.0f);
        time = 0.0f;
        status.mag_rotationality = 1.0f;
        while (time < 1.0f)
        {
            magnification = Mathf.Lerp(_magnification, 1.0f, time);
            cubedMagni = Mathf.Pow(magnification, 3.0f);
            transform.localScale = magnification * _scale;
            status.default_mass = _mass * cubedMagni;
            rg.mass = status.mass;
            status.default_force = cubedMagni * _force;
            Height = magnification * _height - 0.5f * (magnification - 1.0f);
            //status.mag_rotationality = Mathf.Lerp(100.0f, 1.0f, time);
            time += Time.deltaTime;
            yield return null;
        }
        StopSpecialSkill();
    }

    protected override void StopSpecialSkill()
    {
        base.StopSpecialSkill();
        transform.localScale = _scale;
        status.default_mass = _mass;
        rg.mass = status.mass;
        status.default_force = _force;
        Height = _height;
        status.mag_rotationality = 1.0f;
        InstantiateObject.Instance.Cameras[status.PlayerNum].GetComponent<CameraControl>().Cart3StopSpecialSkill();
    }
}
