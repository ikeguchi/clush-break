﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Type2 : PlayerControl {
    public GameObject Wave;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        DefaultLoop();
	}

    protected override IEnumerator SpecialSkill()
    {
        return ShockWave();
    }

    IEnumerator ShockWave()
    {
        Wave.SetActive(true);
        yield return new WaitForSeconds(10.0f);
        Wave.SetActive(false);
        base.StopSpecialSkill();
    }

    protected override void StopSpecialSkill()
    {
        base.StopSpecialSkill();
        Wave.SetActive(false);
    }
}
