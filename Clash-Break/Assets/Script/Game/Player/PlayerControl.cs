﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : HaveCoroutine
{
    protected bool Usefrag = true;
    [SerializeField]
    protected float Height;
    [SerializeField] private AbstractBusterControl buster;
    int _dashcount;
    private string Haxis, Vaxis, dash, SS, Submit;
    Ray ray;
    protected Rigidbody rg;
    protected PlayerStatus status;
    private TransparentControl transparent;
    private Vector3 ResurrectionPos;
    private Quaternion ResurrectionQuat;
    public Vector3 PrevVelocity { get; set; }
    public Rigidbody Rg { get { return rg; } }
    public bool IsClash { get; set; }
    public bool Invulnerable { get; set; }
    private bool _isClashMaterial;
    [SerializeField] private Renderer[] _renderers;
    [SerializeField] private Renderer[] _subrenderer;

    [SerializeField] private Material[] _materials;
    [SerializeField] private Material[] _submaterials;
    [SerializeField] private GameObject ResurrectionLight;

    protected enum CoroutineID
    {
        WaitClash,
        SS,
        Dash,
        DeathMove,
        Clashed
    }
    public bool IsClashMaterial
    {
        get { return _isClashMaterial; }
        set
        {
            if (!_isClashMaterial)
            {
                _isClashMaterial = value;
                StartCoroutineAndSave(WaitClash(), (int)CoroutineID.WaitClash);
                //StartCoroutine(WaitClashCoroutine=WaitClash());
            }
        }
    }

    IEnumerator WaitClash()
    {
        yield return new WaitForSeconds(0.5f);
        _isClashMaterial = false;
        //WaitClashCoroutine = null;
    }
    // Use this for initialization
    private void Awake()
    {
        status = GetComponent<PlayerStatus>();
        status.ParameterReset();
        ray = new Ray(transform.position, Vector3.down);
        rg = GetComponent<Rigidbody>();
        rg.maxAngularVelocity = 30.0f;
        transparent = GetComponent<TransparentControl>();

        if (status == null)
            Debug.LogError("statusがアタッチされていません");
        buster.ChangeState(BusterState.Default);
        Invulnerable = false;
    }

    private void FixedUpdate()
    {
        IsClash = false;
    }

    public PlayerStatus GetStatus()
    {
        return status;
    }

    public virtual void SetPlayerNum(int num)
    {
        Haxis = "Horizontal" + GameState.Instance.ControllerNum[num];
        Vaxis = "Vertical" + GameState.Instance.ControllerNum[num];
        dash = "Dash" + GameState.Instance.ControllerNum[num];
        SS = "SpecialSkill" + GameState.Instance.ControllerNum[num];
        Submit = "ItemUse" + GameState.Instance.ControllerNum[num];
        status.PlayerNum = num;
        GameInfo.Instance.Player[num] = this;
        GameInfo.Instance.attackplayernum[num] = -1;
        buster.Init(num);
        ChangeMaterial(num);
        enabled = false;
    }
    protected void DefaultLoop()
    {
        if (status.IsDeath == false)
        {
            Float();
            Move();
            Death();
            PrevVelocity = rg.velocity;
        }
    }
    void Float()
    {
        ray.origin = transform.position;
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Height * 1.05f, LayerMask.GetMask("Floor")))
        {
            rg.useGravity = false;
            rg.drag = 1.0f;
            transform.position = hit.point + Vector3.up * Height;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(transform.forward - Vector3.Dot(transform.forward, hit.normal) * hit.normal,
                    hit.normal), Time.deltaTime * 10.0f);
        }
        else
        {
            rg.useGravity = true;
            rg.drag = 0.2f;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(transform.forward - Vector3.Dot(transform.forward, Vector3.up) * Vector3.up,
                    Vector3.up), Time.deltaTime);
        }
    }

    void Move()
    {
        BusterState busterState = BusterState.Default;
        if (Input.GetButtonDown(SS))
            if (!GameInfo.Instance.isSSuse[status.PlayerNum])
            {
                GameInfo.Instance.isSSuse[status.PlayerNum] = true;
                InstantiateObject.Instance.Cameras[status.PlayerNum].GetComponent<CameraControl>().UseSS();
                StartCoroutineAndSave(SpecialSkill(), (int)CoroutineID.SS);
                //StartCoroutine(SSCoroutine = SpecialSkill());
            }
        if (Input.GetButtonDown(dash) && status.IsDash == false)
            if (status.DashCount > 0 || Usefrag == false)
                StartCoroutineAndSave(Dash(), (int)CoroutineID.Dash);
        //StartCoroutine(DashCoroutine = Dash());
        if (Input.GetButtonDown(Submit) && status.HavingItem != ItemType.None)
        {
            status.GetEffect(status.HavingItem);
            status.HavingItem = ItemType.None;
        }
        float vertical = 0.0f;
        if (!status.IsClashed)
        {
            vertical = Input.GetAxis(Vaxis);
            rg.AddRelativeForce(Vector3.forward * rg.mass * 1000.0f * Time.deltaTime);
        }
        float horizontal = Input.GetAxis(Haxis);
        if (Mathf.Abs(horizontal) > 0.0f)
        {
            busterState = horizontal > 0.0f ? BusterState.TurnRight : BusterState.TurnLeft;
            rg.AddTorque(Vector3.up * horizontal * 10000.0f * status.rotationality);
            //rg.angularDrag = 1.0f;
            //transform.Rotate(Vector3.up * horizontal * 50.0f * status.rotationality * Time.deltaTime);
            //rg.drag = 2.0f;
        }
        if (status.IsDash)
            busterState = BusterState.Dash;
        if (vertical >= 0.0f)
            rg.AddRelativeForce(Vector3.forward * 250000.0f * vertical * status.force * Time.deltaTime);
        else if (!rg.useGravity)
        {
            rg.drag = 1.0f - 3.0f * vertical;
            busterState = status.IsDash ? BusterState.Default : BusterState.Brake;
        }
        if (!status.IsClashed)
            buster.ChangeState(busterState);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player") && !IsClash)
        {
            IsClash = true;
            var enemy = collision.gameObject.GetComponent<PlayerControl>();
            Attacked(enemy.status.PlayerNum);
            var dir = collision.transform.position - transform.position;
            dir = dir.normalized;
            float speed, impact;
            ClashSpeed(dir, PrevVelocity, enemy.PrevVelocity, out speed, out impact);
            if (speed < 0.0f)
                return;
            collision.rigidbody.AddForce(dir * status.power * speed * 40000.0f / enemy.status.defence);
            if (impact > 50.0f)
            {
                AudioControlinGame.Instance.PlaySE(SEList.StrongClash);
                InstantiateObject.Instance.CreateEffect(EffectType.StrongClashEffect, collision.contacts[0].point, Quaternion.FromToRotation(Vector3.zero, dir));
                enemy.Clashed(1.0f);
            }
            else if (impact > 10.0f)
            {
                AudioControlinGame.Instance.PlaySE(SEList.WeakClash);
                InstantiateObject.Instance.CreateEffect(EffectType.ClashEffect, collision.contacts[0].point, Quaternion.FromToRotation(Vector3.zero, dir));
            }
            else
            {
                AudioControlinGame.Instance.PlaySE(SEList.WeakClash);
            }
        }
    }
    void ClashSpeed(Vector3 dir, Vector3 myVelocity, Vector3 otherVelocity, out float speed, out float impact)
    {
        var myspeed = Mathf.Cos(Vector3.Angle(myVelocity, dir) * Mathf.Deg2Rad) * myVelocity.magnitude;
        var otherspeed = Mathf.Cos(Vector3.Angle(otherVelocity, dir) * Mathf.Deg2Rad) * otherVelocity.magnitude;
        speed = myspeed + otherspeed;
        impact = myspeed - otherspeed;
    }
    IEnumerator Dash()
    {
        AudioControlinGame.Instance.PlaySE(SEList.Bust);
        status.IsDash = true;
        if (Usefrag)
            status.DashCount--;
        float time = 0.0f;
        rg.velocity += transform.forward * 10.0f;
        while (time < 1.0f)
        {
            rg.AddForce(transform.forward * 5000.0f * status.mass * status.force * Time.deltaTime);
            time += Time.deltaTime;
            yield return null;
        }
        status.IsDash = false;
    }
    protected virtual IEnumerator SpecialSkill()
    {
        Debug.LogError("PlayerControlで呼び出された");
        return null;
    }

    protected virtual void StopSpecialSkill()
    {
        StopCoroutineAndRemove((int)CoroutineID.SS);
        InstantiateObject.Instance.Cameras[status.PlayerNum].GetComponent<CameraControl>().EndSS();
    }

    public void Death()
    {
        if (transform.position.y < -5.0f || Vector3.Distance(Vector3.zero, transform.position) > 100.0f&&!status.IsDeath)
        {
            GameState.Instance.Score[status.PlayerNum] -= 1;
            if (GameInfo.Instance.attackplayernum[status.PlayerNum] != -1)
                GameState.Instance.Score[GameInfo.Instance.attackplayernum[status.PlayerNum]] += 2;
            if (FindCoroutine((int)CoroutineID.SS))
                StopSpecialSkill();
            status.ParameterReset();
            status.IsDash = false;
            InstantiateObject.Instance.Reset(status.PlayerNum, out ResurrectionPos, out ResurrectionQuat);
            status.IsDeath = true;
            transparent.Transparent(true, true);
            status.IsTransparent = true;
            rg.useGravity = false;
            rg.Sleep();
            gameObject.layer = LayerMask.NameToLayer("Dead");
            StartCoroutineAndSave(DeathMove(), (int)CoroutineID.DeathMove);
        }
    }

    IEnumerator DeathMove()
    {
        Vector3 pos = transform.position;
        Quaternion quat = transform.rotation;
        ResurrectionLight.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        AudioControlinGame.Instance.PlaySE(SEList.Resurrection);
        float time = 0.0f;
        while (time < 2.0f)
        {
            time += Time.deltaTime;
            transform.position = Vector3.Slerp(pos, ResurrectionPos, time / 2.0f);
            transform.rotation = Quaternion.Lerp(quat, ResurrectionQuat, time / 2.0f);
            yield return null;
        }
        transform.position = ResurrectionPos;
        transform.rotation = ResurrectionQuat;
        status.IsDeath = false;
        transparent.Transparent(false);
        rg.WakeUp();
        status.IsTransparent = false;
        gameObject.layer = LayerMask.NameToLayer("Player");
        rg.velocity = Vector3.zero;
        ResurrectionLight.SetActive(false);
    }

    public void Attacked(int num)
    {
        /*if (GameInfo.Instance.attackplayernum[status.PlayerNum] != -1)
        {
            StopCoroutineAndRemove((int)Col);
        }
        nowAttackedCoroutine = ChangeAttackedNum(num);*/
        GameInfo.Instance.attackplayernum[status.PlayerNum] = num;
        //StartCoroutineAndSave(ChangeAttackedNum(num),(int)CoroutineID.nowAttacked);
    }
    /*public IEnumerator ChangeAttackedNum(int num)
    {
        GameInfo.Instance.attackplayernum[status.PlayerNum] = num;
        yield return new WaitForSeconds(5.0f);
        GameInfo.Instance.attackplayernum[status.PlayerNum] = -1;
    }*/

    public bool GetItem(ItemType type)
    {
        if (status.HavingItem != ItemType.None)
            return false;
        AudioControlinGame.Instance.PlaySE(SEList.Itemget);
        status.HavingItem = type;
        return true;
    }

    public void Clashed(float time)
    {
        StartCoroutineAndSave(_Clashed(time), (int)CoroutineID.Clashed);
    }
    IEnumerator _Clashed(float time)
    {
        buster.ChangeState(BusterState.Stop);
        status.IsClashed = true;
        yield return new WaitForSeconds(time);
        status.IsClashed = false;
        if (!status.IsDeath && !status.IsTransparent)
            buster.ChangeState(BusterState.Play);
    }

    public void Stop()
    {
        rg.velocity = Vector3.zero;
        rg.useGravity = false;
        rg.Sleep();
        enabled = false;
    }

    void ChangeMaterial(int num)
    {
        foreach (var render in _renderers)
        {
            render.material = _materials[num];
        }
        foreach (var render in _subrenderer)
        {
            render.material = _submaterials[num];
        }
    }
}
