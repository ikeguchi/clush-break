﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Type4 : PlayerControl
{
    private float force;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        DefaultLoop();
	}

    protected override IEnumerator SpecialSkill()
    {
        return InftyDash();
    }
    IEnumerator InftyDash()
    {
        status.IsSSDash = true;
        Usefrag = false;
        force = status.default_force;
        status.mag_power += 1;
        status.default_force *= 2;
        yield return new WaitForSeconds(10.0f);
        status.default_force /= 2;
        status.mag_power -= 1;
        Usefrag = true;
        status.IsSSDash = false;
        base.StopSpecialSkill();
    }

    protected override void StopSpecialSkill()
    {
        base.StopSpecialSkill();
        status.default_force = force;
        Usefrag = true;
        status.IsSSDash = false;
    }
}
