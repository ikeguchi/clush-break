﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Type1 : PlayerControl {
    public GameObject Shield;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        DefaultLoop();
	}
    protected override IEnumerator SpecialSkill()
    {
        return CreateShield();
    }
    
    IEnumerator CreateShield()
    {
        float time = 0.0f;
        Shield.SetActive(true);
        Invulnerable = true;
        var scale = Shield.transform.localScale;
        while (time < 1.0f)
        {
            Shield.transform.localScale = Vector3.Slerp(Vector3.zero, scale, time);
            time += Time.deltaTime;
            yield return null;
        }
        Shield.transform.localScale = scale;
        yield return new WaitForSeconds(5.0f);
        time = 0.0f;
        while (time < 1.0f)
        {
            Shield.transform.localScale = Vector3.Slerp(scale, Vector3.zero, time);
            time += Time.deltaTime;
            yield return null;
        }
        Shield.SetActive(false);
        Invulnerable = false;
        base.StopSpecialSkill();
    }

    protected override void StopSpecialSkill()
    {
        base.StopSpecialSkill();
        Shield.SetActive(false);
        Invulnerable = false;
    }
}
