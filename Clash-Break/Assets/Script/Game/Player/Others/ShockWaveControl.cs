﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockWaveControl : MonoBehaviour {
    PlayerStatus parent;
    private SphereCollider ShockWaveCollider;
    private ParticleSystem pt;
    ParticleSystem.Particle[] par=new ParticleSystem.Particle[2];

	// Use this for initialization
	void Start () {
        parent = GetComponentInParent<PlayerStatus>();
	    ShockWaveCollider = GetComponent<SphereCollider>();
	    pt = GetComponent<ParticleSystem>();
	}

    void Update()
    { 
        pt.GetParticles(par);
        ShockWaveCollider.radius = pt.sizeOverLifetime.size.Evaluate(ElapsedTimeRate(par[0])) * 20.0f;
    }

    float ElapsedTimeRate(ParticleSystem.Particle _par)
    {
        return (_par.startLifetime - _par.remainingLifetime) / _par.startLifetime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var enemy = other.GetComponentInParent<PlayerControl>();
            var dir = other.transform.position - transform.position;
            dir = dir.normalized;
            enemy.Rg.AddForce(dir * 1000000.0f * parent.power);
            enemy.Attacked(parent.PlayerNum);
        }
    }
}