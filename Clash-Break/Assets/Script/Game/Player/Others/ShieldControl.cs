﻿using UnityEngine;

public class ShieldControl : MonoBehaviour
{
    private PlayerStatus parent;

    // Use this for initialization
    private void Start()
    {
        parent = transform.parent.GetComponent<PlayerStatus>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioControlinGame.Instance.PlaySE(SEList.Shield);
            var enemy = other.gameObject.GetComponentInParent<PlayerControl>();
            enemy.Attacked(parent.PlayerNum);
            var dir = other.transform.position - transform.position;
            dir = dir.normalized;
            other.attachedRigidbody.velocity = other.attachedRigidbody.velocity.magnitude * dir +
                                               GetComponentInParent<Rigidbody>().velocity;
        }
    }
}