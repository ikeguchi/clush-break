﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentControl : MonoBehaviour
{
    [SerializeField]private Renderer[] renderers;
    [SerializeField] private ParticleSystem[] stopParticleSystems;
	// Use this for initialization

    public void Transparent(bool isTransparent, bool isDeath = false)
    {
        if (isTransparent)
        {
            float alpha = isDeath ? 0.0f : 0.05f;
            foreach (var render in renderers)
            {
                foreach (var mat in render.materials)
                {
                    SetupMaterialWithBlendMode(mat, BlendMode.Fade);
                    Color col = mat.color;
                    col.a = alpha;
                    mat.color = col;
                }
            }
            foreach (var particle in stopParticleSystems)
            {
                particle.Stop(true);
            }
        }
        else
        {
            foreach (var render in renderers)
            {
                foreach (var mat in render.materials)
                {
                    Color col = mat.color;
                    col.a = 1.0f;
                    mat.color = col;
                    SetupMaterialWithBlendMode(mat, BlendMode.Opaque);
                }
            }
            foreach (var particle in stopParticleSystems)
            {
                particle.Play(true);
            }
        }
    }

    enum BlendMode
    {
        Opaque,
        Cutout,
        Fade,        // Old school alpha-blending mode, fresnel does not affect amount of transparency
        Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
    }

    void SetupMaterialWithBlendMode(Material material, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                material.SetOverrideTag("RenderType", "");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                material.SetOverrideTag("RenderType", "TransparentCutout");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.EnableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 2450;
                break;
            case BlendMode.Fade:
                material.SetOverrideTag("RenderType", "Transparent");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
            case BlendMode.Transparent:
                material.SetOverrideTag("RenderType", "Transparent");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
        }
    }
}
