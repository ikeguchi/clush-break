﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBusterControl:MonoBehaviour{
    private BusterState _nowState;
    public void ChangeState(BusterState state)
    {
        if (state == _nowState)
            return;
        _nowState = state;
        switch (state)
        {
            case BusterState.Default:
                Default();
                break;
            case BusterState.Dash:
                Dash();
                break;
            case BusterState.TurnLeft:
                TurnLeft();
                break;
            case BusterState.TurnRight:
                TurnRight();
                break;
            case BusterState.Brake:
                Brake();
                break;
            case BusterState.Stop:
                Stop();
                break;
            case BusterState.Play:
                Play();
                break;
        }
    }
    protected abstract void Default();
    protected abstract void Dash();
    protected abstract void TurnLeft();
    protected abstract void TurnRight();
    protected abstract void Brake();
    protected abstract void Stop();
    protected abstract void Play();

    public virtual void Init(int id)
    {}
}

public class BusterControl : AbstractBusterControl
{
    [SerializeField] private BustControl[] RightBusters,LeftParticles,CenterParticles;

    protected override void Default()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Default();
        }
        foreach (var buster in RightBusters)
        {
            buster.Default();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Default();
        }
    }

    protected override void Dash()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Strong();
        }
        foreach (var buster in RightBusters)
        {
            buster.Strong();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Strong();
        }
    }

    protected override void TurnLeft()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Week();
        }
        foreach (var buster in RightBusters)
        {
            buster.Strong();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Week();
        }
    }

    protected override void TurnRight()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Strong();
        }
        foreach (var buster in RightBusters)
        {
            buster.Week();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Week();
        }
    }

    protected override void Brake()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Week();
        }
        foreach (var buster in RightBusters)
        {
            buster.Week();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Week();
        }
    }

    protected override void Stop()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Stop();
        }
        foreach (var buster in RightBusters)
        {
            buster.Stop();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Stop();
        }
    }

    protected override void Play()
    {
        foreach (var buster in LeftParticles)
        {
            buster.Play();
        }
        foreach (var buster in RightBusters)
        {
            buster.Play();
        }
        foreach (var buster in CenterParticles)
        {
            buster.Play();
        }
    }
}

public enum BusterState
{
    Default,
    Dash,
    TurnLeft,
    TurnRight,
    Brake,
    Stop,
    Play
}
