﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cart1Buster : AbstractBusterControl
{
    [SerializeField] private Renderer[] _busterRenderer;
    [SerializeField] private ParticleSystem[] _pt;
    private Color _col;

    protected override void Default()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col * 0.755f);
        }
        foreach (var particle in _pt)
        {
            var main = particle.main;
            var curve = main.startLifetime;
            curve.constant = 0.3f;
            main.startLifetime = curve;
        }
    }

    protected override void Dash()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col);
        }
        foreach (var particle in _pt)
        {
            var main = particle.main;
            var curve = main.startLifetime;
            curve.constant = 0.5f;
            main.startLifetime = curve;
        }
    }

    protected override void TurnLeft()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col * 0.7f);
        }
        var main = _pt[1].main;
        var curve = main.startLifetime;
        curve.constant = 0.5f;
        main.startLifetime = curve;
    }

    protected override void TurnRight()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col * 0.7f);
        }
        var main = _pt[0].main;
        var curve = main.startLifetime;
        curve.constant = 0.5f;
        main.startLifetime = curve;
    }

    protected override void Brake()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col * 0.2f);
        }
        foreach (var particle in _pt)
        {
            var main = particle.main;
            var curve = main.startLifetime;
            curve.constant = 0.2f;
            main.startLifetime = curve;
        }
    }

    protected override void Stop()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", Color.black);
        }
        foreach (var particle in _pt)
        {
            particle.Stop();
        }
    }

    protected override void Play()
    {
        //_busterRenderer.material.EnableKeyword("_EMISSION");
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col * 0.755f);
        }
        foreach (var particle in _pt)
        {
            particle.Play();
        }
    }

    public override void Init(int id)
    {
        _col = GameInfo.Instance.PlayerColors[id];
        foreach (var buster in _busterRenderer)
        {
            buster.material.SetColor("_EmissionColor", _col * 0.755f);
        }
        foreach (var particle in _pt)
        {
            ParticleSystem.MainModule main = particle.main;
            main.startColor = _col;
        }
    }
}
