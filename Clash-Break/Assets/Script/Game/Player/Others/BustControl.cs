﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BustControl : MonoBehaviour
{
    private ParticleSystem[] _fire;
    private float[] _defaultLifeTime;
    private void Start()
    {
        _fire = GetComponentsInChildren<ParticleSystem>();
        _defaultLifeTime=new float[2];
        for (int i = 0; i < _fire.Length; i++)
        {
            _defaultLifeTime[i] = _fire[i].main.startLifetime.constant;
        }
        //_defaultLifeTime[0] = fire.main.startLifetime.constant;
        //_defaultLifeTime[1] = fire.main.startLifetime.constant;
    }

    public void Week()
    {
        /*var curve = fire.main.startLifetime;
        curve.constant = _defaultLifeTime[0] / 2;
        var curve1 = glow.main.startLifetime;
        curve1.constant = _defaultLifeTime[1] / 2;*/
        for (int i = 0; i < _fire.Length; i++)
        {
            var mainModule = _fire[i].main;
            var curve = mainModule.startLifetime;
            curve.constant = _defaultLifeTime[i] / 2;
            mainModule.startLifetime = curve;
        }
    }

    public void Default()
    {
        /*var curve = fire.main.startLifetime;
        curve.constant = _defaultLifeTime[0];
        var curve1 = glow.main.startLifetime;
        curve1.constant = _defaultLifeTime[1];*/
        for (int i = 0; i < _fire.Length; i++)
        {
            var mainModule = _fire[i].main;
            var curve = mainModule.startLifetime;
            curve.constant = _defaultLifeTime[i];
            mainModule.startLifetime = curve;
        }
    }

    public void Strong()
    {
        /*var curve = fire.main.startLifetime;
        curve.constant = _defaultLifeTime[0] * 1.5f;
        var curve1 = glow.main.startLifetime;
        curve1.constant = _defaultLifeTime[1] * 1.5f;*/
        for (int i = 0; i < _fire.Length; i++)
        {
            var mainModule = _fire[i].main;
            var curve = mainModule.startLifetime;
            curve.constant = _defaultLifeTime[i] * 1.5f;
            mainModule.startLifetime = curve;
        }
    }

    public void Stop()
    {
        foreach (var par in _fire)
        {
            par.Stop();
        }
    }

    public void Play()
    {
        foreach (var par in _fire)
        {
            par.Play();
        }
    }
}
