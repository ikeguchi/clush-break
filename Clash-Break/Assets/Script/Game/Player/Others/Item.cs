﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ItemEffect
{
    protected float power, defence;
    protected PlayerStatus status;
    protected float StartTime;
    public float RemainingTime { get; protected set; }
    public ItemType Type { get; set; }

    public virtual bool ItemUpdate()
    {
        RemainingTime -= Time.deltaTime;
        if (RemainingTime < 0.0f)
        {
            DestroyEffect();
            return true;
        }
        return false;
    }

    public virtual void DestroyEffect()
    {
        status.mag_power -= power;
        status.mag_defence -= defence;
    }

    protected virtual void Affect()
    {
        status.mag_power += power;
        status.mag_defence += defence;
    }

    public float RemainingRate()
    {
        return RemainingTime / StartTime;
    }
}

public sealed class ChangeAttack : ItemEffect
{
    public ChangeAttack(float str,float time,int num)
    {
        power = str;
        RemainingTime = time;
        defence = 0.0f;
        status = GameInfo.Instance.Player[num].GetComponent<PlayerStatus>();
        Affect();
        Type = str > 0.0f ? ItemType.AttackUp : ItemType.AttackDown;
        StartTime = time;
    }
}

public sealed class ChangeDefence : ItemEffect
{
    public ChangeDefence(float def, float time, int num)
    {
        defence = def;
        RemainingTime = time;
        power = 0.0f;
        status = GameInfo.Instance.Player[num].GetComponent<PlayerStatus>();
        Affect();
        Type = def > 0.0f ? ItemType.DefenceUp : ItemType.DefenceDown;
        StartTime = time;
    }
}

public class TransparentItem : ItemEffect
{
    public TransparentItem(float time, int num)
    {
        RemainingTime = time;
        status = GameInfo.Instance.Player[num].GetComponent<PlayerStatus>();
        Affect();
        Type = ItemType.Transparent;
        StartTime = time;
    }

    protected sealed override void Affect()
    {
        status.GetComponent<TransparentControl>().Transparent(true);
        status.IsTransparent = true;
    }

    public override void DestroyEffect()
    {
        status.GetComponent<TransparentControl>().Transparent(false);
        status.IsTransparent = false;
    }
}

public class InkScreen : ItemEffect
{
    private readonly VideoPlayer video;
    public InkScreen(float time, int num)
    {
        RemainingTime = time;
        status = GameInfo.Instance.Player[num].GetComponent<PlayerStatus>();
        video = InstantiateObject.Instance.Cameras[status.PlayerNum].GetComponentInChildren<VideoPlayer>();
        Affect();
        Type = ItemType.InkScreen;
        StartTime = time;
    }

    protected sealed override void Affect()
    {
        video.targetCameraAlpha = 1.0f;
        video.Play();
    }

    public override void DestroyEffect()
    {
        video.Stop();
    }

    public override bool ItemUpdate()
    {
        if (RemainingTime < 1.0f)
            video.targetCameraAlpha = RemainingTime;
        return base.ItemUpdate();
    }
}