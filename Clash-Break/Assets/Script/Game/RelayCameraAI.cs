﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelayCameraAI : HaveCoroutine
{
    public RenderTexture ScreenRenderTexture;
    public RelayCameraControl[] Cameras;
    int _maxCameranum;
    private int _playernum, _cameranum;
    private float _time;
	// Use this for initialization
	void Start ()
	{
	    _maxCameranum = Cameras.Length - 1;
	    _cameranum = Random.Range(0, _maxCameranum);
	    Cameras[_cameranum].enabled = true;
	    _playernum = Random.Range(0, GameState.Instance.PlayerNum - 1);
	    Cameras[_cameranum].SetCamera(_playernum, ScreenRenderTexture);
	    StartCoroutineAndSave(IntervalTime(), 0);
	    //StartCoroutine(IntervalTime());
	    _time = 0.0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _time += Time.deltaTime;
	    /*if (GameInfo.Instance.Player[_playernum] == null)
	    {
            StopCoroutine(_interval);
	        while (ChangePlayer(Random.Range(0, GameState.Instance.PlayerNum - 1)))
	        {
	        }
	    }*/
	    if (_time > 5.0f)
	        ChangeRelayCamera(Random.Range(0, _maxCameranum));
	}

    IEnumerator IntervalTime()
    {
        yield return new WaitForSeconds(10.0f);
        ChangePlayer(Random.Range(0, GameState.Instance.PlayerNum - 1));
    }

    void ChangeRelayCamera(int num)
    {
        if(_cameranum==num)
            return;
        Cameras[_cameranum].ReleaseCamera();
        Cameras[num].enabled = true;
        Cameras[num].SetCamera(_playernum, ScreenRenderTexture);
        _cameranum = num;
        _time = 0.0f;
    }

    void ChangePlayer(int num)
    {
        if(_playernum==num)
            return;
        Cameras[_cameranum].SetCamera(num);
        _playernum = num;
        StartCoroutineAndSave(IntervalTime(), 0);
        _time = 0.0f;
    }
}
