﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class RigidbodyVelocity
{
    public Vector3 velocity;
    public Vector3 angularVeloccity;
    public RigidbodyVelocity(Rigidbody rigidbody)
    {
        velocity = rigidbody.velocity;
        angularVeloccity = rigidbody.angularVelocity;
    }
}

public class Pausable : MonoBehaviour
{
    bool isPause = false;
    public GameObject PausePanel;
    public GameObject[] ignoreGameObjects;
    RigidbodyVelocity[] rigidbodyVelocities;
    [SerializeField] private Image pausingImage;

    Rigidbody[] pausingRigidbodies;
    MonoBehaviour[] pausingMonoBehaviours;
    ParticleSystem[] pausingParticleSystem;
    AudioSource[] pausingAudioSource;
    private Animator[] pausingAnimators;
    private Animation[] pausingAnimation;

    void Update()
    {
        if (GameInfo.Instance.isGamePlaying)
        {
            for (int i = 0; i < GameState.Instance.PlayerNum; i++)
                if (Input.GetButtonDown("Select" + GameState.Instance.ControllerNum[i]))
                    ChangeState(i);
        }
    }
    void ChangeState(int num)
    {
        if (!isPause)
        {
            isPause = true;
            GameInfo.Instance.PausingPlayer = num + 1;
            pausingImage.sprite = Resources.Load<Sprite>("Images/" + (num + 1) + "P");
            PausePanel.SetActive(true);
            GameObject.FindGameObjectWithTag("EventSystem").GetComponent<ButtonManager>().SetKey();
            Pause();
        }
        else if (GameInfo.Instance.PausingPlayer == num + 1)
        {
            Resume();
            PausePanel.SetActive(false);
            isPause = false;
        }
    }

    public void ForcedResume()
    {
        if (isPause)
        {
            Resume();
            PausePanel.SetActive(false);
            isPause = false;
        }
    }
    void Pause()
    {
        //無視したいオブジェクトがあるとき
        Predicate<Rigidbody> rigidbodyPredicate =
            obj => !obj.IsSleeping() &&
                   Array.FindIndex(ignoreGameObjects, _obj => _obj == obj.gameObject) < 0;
        pausingRigidbodies = Array.FindAll(transform.GetComponentsInChildren<Rigidbody>(true), rigidbodyPredicate);
        rigidbodyVelocities = new RigidbodyVelocity[pausingRigidbodies.Length];
        for (int i = 0; i < pausingRigidbodies.Length; i++)
        {
            // 速度、角速度も保存しておく
            rigidbodyVelocities[i] = new RigidbodyVelocity(pausingRigidbodies[i]);
            pausingRigidbodies[i].Sleep();
        }

        //無視したいオブジェクトがあるとき
        Predicate<MonoBehaviour> monoBehaviourPredicate =
            obj => obj.enabled &&
                   obj != this &&
                   Array.FindIndex(ignoreGameObjects, _obj => _obj == obj.gameObject) < 0;
        pausingMonoBehaviours = Array.FindAll(transform.GetComponentsInChildren<MonoBehaviour>(true), monoBehaviourPredicate);
        foreach (var monoBehaviour in pausingMonoBehaviours)
        {
            monoBehaviour.enabled = false;
        }
        Predicate<ParticleSystem> ptPredicate = pt => pt.isPlaying;
        pausingParticleSystem = Array.FindAll(GetComponentsInChildren<ParticleSystem>(), ptPredicate);
        foreach (var particle in pausingParticleSystem)
            particle.Pause();
        Predicate<AudioSource> audioPredicate = audio => audio.isPlaying;
        pausingAudioSource = Array.FindAll(GetComponentsInChildren<AudioSource>(), audioPredicate);
        foreach (var audio in pausingAudioSource)
            audio.Pause();
        Predicate<Animation> animationPredicate = anim => anim.isPlaying &&
                                                          Array.FindIndex(ignoreGameObjects,
                                                              _obj => _obj == anim.gameObject) < 0;
        pausingAnimation = Array.FindAll(GetComponentsInChildren<Animation>(), animationPredicate);
        foreach (var anim in pausingAnimation)
        {
            anim.Stop();
        }
        pausingAnimators = GetComponentsInChildren<Animator>();
        foreach (var anim in pausingAnimators)
        {
            anim.enabled = false;
        }
    }

    void Resume()
    {
        for (int i = 0; i < pausingRigidbodies.Length; i++)
        {
            pausingRigidbodies[i].WakeUp();
            pausingRigidbodies[i].velocity = rigidbodyVelocities[i].velocity;
            pausingRigidbodies[i].angularVelocity = rigidbodyVelocities[i].angularVeloccity;
        }

        foreach (var monoBehaviour in pausingMonoBehaviours)
        {
            monoBehaviour.enabled = true;
        }

        foreach (var particle in pausingParticleSystem)
        {
            particle.Play();
        }
        foreach (var audio in pausingAudioSource)
            audio.Play();
        foreach (var anim in pausingAnimation)
        {
            anim.Play();
        }
        foreach (var anim in pausingAnimators)
        {
            anim.enabled = true;
        }
    }
}