﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SSControl : MonoBehaviour
{
    private Image ssImage;
    private float time = 0;
	// Use this for initialization
	void Start ()
	{
	    ssImage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
	    var col = ssImage.color;
	    col.a = 0.5f * (1 + Mathf.Cos(time * Mathf.PI));
	    ssImage.color = col;
	    time += Time.deltaTime;
	    if (time > 2.0f)
	        time -= 2.0f;
    }
}
