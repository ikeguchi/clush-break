﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerControl : MonoBehaviour
{
    private Image gage;
    //private Text timeText;
    private int minite, second;
    private float startTime;

    // Use this for initialization
    void Start()
    {
        gage = GetComponent<Image>();
        //timeText = GetComponentInChildren<Text>();
        GameInfo.Instance.isGamePlaying = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameInfo.Instance.isGamePlaying)
        {
            GameInfo.Instance.RemainingTime -= Time.deltaTime;
            gage.fillAmount = GameInfo.Instance.RemainingTime / startTime;
            Sec2Time(GameInfo.Instance.RemainingTime);
            //timeText.text = minite.ToString("00") + ":" + second.ToString("00");
            if (GameInfo.Instance.RemainingTime < 0.0f)
                GameEnd();
        }
    }

    public void StartCount(float sec)
    {
        minite = (int)sec / 60;
        second = (int)sec % 60;
        GameInfo.Instance.RemainingTime = sec;
        startTime = sec;
        //timeText.text = minite.ToString("00") + ":" + second.ToString("00");
        StartCoroutine(GameStart());
    }

    IEnumerator GameStart()
    {
        yield return new WaitForSeconds(3.0f);
        GameInfo.Instance.isGamePlaying = true;
        InstantiateObject.Instance.GameStart();
    }

    void GameEnd()
    {
        GameInfo.Instance.isGamePlaying = false;
        InstantiateObject.Instance.GameEnd();
    }

    void Sec2Time(float sec)
    {
        minite = (int)sec / 60;
        second = (int)sec % 60;
    }
}
