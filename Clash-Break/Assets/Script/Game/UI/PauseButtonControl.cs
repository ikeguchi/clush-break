﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseButtonControl : MonoBehaviour
{
    [SerializeField] private GameObject _firstselect;
    private void OnEnable()
    {
        GameObject.FindGameObjectWithTag("EventSystem").GetComponent<EventSystem>().firstSelectedGameObject =
            _firstselect;
    }
}
