﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectUI : HaveCoroutine
{
    public GameObject[] PanelObjects;
    public GameObject DashPanel;
    /// <summary>
    /// 0:panel
    /// 1:item
    /// 2:gage
    /// </summary>
    private Image[][] itemImages = new Image[3][];
    private Image dashPanelgage;
    private Text dashCountText;
    private bool isDash;
	// Use this for initialization
	void Start () {
	    for (int i = 0; i < PanelObjects.Length; i++)
	    {
	        itemImages[i] = PanelObjects[i].GetComponentsInChildren<Image>();
	    }
	    dashPanelgage = DashPanel.GetComponentsInChildren<Image>()[1];
	    dashCountText = DashPanel.GetComponentInChildren<Text>();
	    isDash = false;
	}

    public void DrawUI(List<ItemEffect> effects, Sprite[] sprites, bool isdash, bool isSSdash, int dashcount)
    {
        int i = 0;
        dashPanelgage.fillAmount = 0.0f;
        foreach (var effect in effects)
        {
            PanelObjects[i].SetActive(true);
            itemImages[i][1].sprite = sprites[(int) effect.Type];
            itemImages[i][2].fillAmount = effect.RemainingRate();
            i++;
            if (i > 2)
                break;
        }
        for (; i < PanelObjects.Length; i++)
        {
            PanelObjects[i].SetActive(false);
        }
        if (isdash&&!isDash)
        {
            StartCoroutineAndSave(UseDash(1.0f), 0);
        }
        else if (dashcount != 0)
            dashPanelgage.fillAmount = 1.0f;
        isDash = isdash;
        dashCountText.text = dashcount.ToString();
    }

    IEnumerator UseDash(float time)
    {
        float remainingtime = time;
        while (remainingtime > 0.0f)
        {
            dashPanelgage.fillAmount = remainingtime / time;
            remainingtime -= Time.deltaTime;
            if (isDash == false)
            {
                yield break;
            }
            yield return null;
        }
    }
}
