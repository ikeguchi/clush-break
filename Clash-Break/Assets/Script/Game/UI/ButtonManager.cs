﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonManager : MonoBehaviour {
    StandaloneInputModule module;
	// Use this for initialization
	void Start () {
        module = GetComponent<StandaloneInputModule>();
	}
    public void SetKey()
    {
        module.horizontalAxis = "Horizontal" + GameInfo.Instance.PausingPlayer;
        module.verticalAxis = "ButtonSelect" + GameInfo.Instance.PausingPlayer;
        module.submitButton = "Submit" + GameInfo.Instance.PausingPlayer;
    }
    public void KeepPlaying()
    {
        GameObject.FindGameObjectWithTag("PausableObject").GetComponent<Pausable>().ForcedResume();
    }
    public void EndPlay()
    {

    }
}
