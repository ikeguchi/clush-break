﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseControl : MonoBehaviour {

    private Text text;
    private float time = 0;
    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        var col = text.color;
        col.a = 0.5f * (1 + Mathf.Cos(time * Mathf.PI));
        text.color = col;
        time += Time.deltaTime;
        if (time > 2.0f)
            time -= 2.0f;
    }
}
