﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : HaveCoroutine
{
    private Animator animator;
    private bool active = true;
    class ItemLate
    {
        public ItemLate(ItemType type,int rate)
        {
            Type = type;
            Rate = rate;
        }
        public ItemType Type { get; private set; }
        public int Rate { get; private set; }
    }

    private readonly ItemLate[] itemLates =
    {
        new ItemLate(ItemType.AttackUp, 30),
        new ItemLate(ItemType.DefenceUp, 30),
        new ItemLate(ItemType.AttackDown, 10),
        new ItemLate(ItemType.DefenceDown, 10),
        new ItemLate(ItemType.Dash, 30),
        new ItemLate(ItemType.Transparent, 20),
        new ItemLate(ItemType.InkScreen, 10)
    };

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (active)
        {
            animator.SetBool("IsAvailable", false);
            ItemType type = ItemType.None;
            int sum = 0;
            foreach (var item in itemLates)
                sum += item.Rate;
            var rate = Random.Range(1, sum);
            foreach (var item in itemLates)
            {
                rate -= item.Rate;
                if (rate <= 0)
                {
                    type = item.Type;
                    break;
                }
            }
            StartCoroutineAndSave(
                other.GetComponentInParent<PlayerControl>().GetItem(type) ? Vanish(7.0f) : Vanish(5.0f), 0);
        }
    }

    IEnumerator Vanish(float time)
    {
        active = false;
        yield return new WaitForSeconds(time);
        animator.SetBool("IsAvailable", true);
        active = true;
    }
}
