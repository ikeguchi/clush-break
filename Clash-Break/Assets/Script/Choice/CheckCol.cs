﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckCol : MonoBehaviour {

    public int playernum;

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetButtonDown("Submit")) {
            if (other.tag == "EventSystem") {
                GameState.Instance.ModelNum[playernum - 1] = int.Parse(other.name);
                Debug.Log(playernum + " choiced " + other.name);
            }
            if(other.tag == "Button") {
                SceneManager.LoadScene("Game");
                //Application.LoadLevel("game");
            }
        }
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
