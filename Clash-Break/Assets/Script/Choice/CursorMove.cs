﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorMove : MonoBehaviour
{


    public int c_num;
    public string cursor_name;
    private int i;
    private float h_move;
    private float v_move;
    private RectTransform cursor;

    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.name);
    }



    void Start()
    {
        i = 0;
        cursor = GameObject.Find(cursor_name).GetComponent<RectTransform>();
    }

    void Update()
    {


        if (GameState.Instance.ControllerNum[c_num - 1] != 0)
        {
            h_move = Input.GetAxis("Horizontal" + GameState.Instance.ControllerNum[c_num - 1]);
            v_move = Input.GetAxis("Vertical" + GameState.Instance.ControllerNum[c_num - 1]);
        }

        i = (h_move == 0 && v_move == 0) ? 4 - c_num : 4;

        this.transform.SetSiblingIndex(i);
        //Debug.Log(cursor_name + "SiblingIndex:" + this.transform.GetSiblingIndex());
        cursor.localPosition += new Vector3(h_move * 3, v_move * 3, 0);
    }
}
