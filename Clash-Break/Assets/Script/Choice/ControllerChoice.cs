﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerChoice : MonoBehaviour {

    [SerializeField]

    const int m_maxPlayerNum = 4;
    private int[] m_array = new int[m_maxPlayerNum];
    private bool[] m_isChoiced = new bool[m_maxPlayerNum];
    private int m_count = 0;        
    
	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        
            for (int i = 0;i < m_maxPlayerNum; i++) {
            if(Input.GetAxisRaw("Vertical" + (i + 1).ToString()) != 0 ||Input.GetAxisRaw("Horizontal" + (i + 1).ToString()) != 0) {
                if (!m_isChoiced[i]) {
                    GameState.Instance.ControllerNum[m_count++] = i + 1;
                    GameState.Instance.PlayerNum++;
                    m_isChoiced[i] = true;
                    break;
                }
            }
        }
        
        
	}
}
