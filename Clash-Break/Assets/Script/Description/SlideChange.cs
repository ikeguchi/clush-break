﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SlideChange : MonoBehaviour {

    public GameObject[] obj;
    public string before_scene_name;
    public string next_scene_name;
    public Text m_text;
    public Text next_text;
    public Text back_text;

    private int count;
    

	// Use this for initialization
	void Start () {
        obj[0].SetActive(true);
        m_text.text = "( 1 / " + obj.Length + " )";
        count = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Submit")) {
            if(count < obj.Length - 1) {
                count++;
                obj[count].SetActive(true);
                obj[count - 1].SetActive(false);
                m_text.text = "( " + (count + 1) + " / " + obj.Length + " )";
               
                if(count != obj.Length - 1) {
                    next_text.text = "2:すすむ";
                    back_text.text = "3:もどる";
                }
            }
            else {
                SceneManager.LoadScene(next_scene_name);
            }
        }
        else if (Input.GetButtonDown("Cancel")) {
            if (count > 0) {
                count--;
                obj[count].SetActive(true);
                obj[count + 1].SetActive(false);
                m_text.text = "( " + (count + 1) + " / 5 )";
                if(count != 0) {
                    next_text.text = "2:すすむ";
                    back_text.text = "3:もどる";
                }
            }
            else {
                SceneManager.LoadScene(before_scene_name);
            }
        }
        if (count == 0) 
            back_text.text = "3:タイトルへ";
        if (count == obj.Length - 1) 
            next_text.text = "2:選択画面へ";
        
    }
}
